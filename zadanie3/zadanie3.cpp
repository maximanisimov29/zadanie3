//я не смог осилить второй процесс, поэтому вот один работающий
#include "stdafx.h"
#include "windows.h"
#include <iostream>
using namespace std;
void main()
{
	setlocale(LC_ALL, "Russian");
	int a;
	STARTUPINFO cif;
	ZeroMemory(&cif, sizeof(STARTUPINFO));
	PROCESS_INFORMATION pi;
	if (CreateProcess("c:\\windows\\notepad.exe", NULL,
		NULL, NULL, FALSE, NULL, NULL, NULL, &cif, &pi) == TRUE)
	{
	
		cout << "Введите требуемый приоритет процесса: " << endl; 
		cout << "1 - Реального времени" << endl;
		cout << "2 - Высокий" << endl;
		cout << "3 - Выше среднего" << endl;               
		cout << "4 - Средний" << endl;
		cout << "5 - Ниже среднего" << endl;
		cout << "6 - IDLE" << endl;
		cin >> a;
		switch (a) {
		case 1: SetPriorityClass(pi.hProcess, REALTIME_PRIORITY_CLASS);      break;
		case 2: SetPriorityClass(pi.hProcess, HIGH_PRIORITY_CLASS);          break;
		case 3: SetPriorityClass(pi.hProcess, ABOVE_NORMAL_PRIORITY_CLASS);  break;
		case 4: SetPriorityClass(pi.hProcess, NORMAL_PRIORITY_CLASS);            break;
		case 5: SetPriorityClass(pi.hProcess, BELOW_NORMAL_PRIORITY_CLASS);  break;
		case 6: SetPriorityClass(pi.hProcess, IDLE_PRIORITY_CLASS);          break;
		}
		cout << "process" << endl;
		cout << "Дескриптор процесса " << pi.hProcess << endl;
		cout << "Дескриптор потока: " << pi.hThread << endl;
		
	}
	system("PAUSE");
}